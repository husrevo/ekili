Result of a hack night and my first exposure to Dropwizard. A REST API to a funny draw for a give away.

Story:
My front-end dev friend Adem Ilter told me about their webmagazin.co is giving away books to 3 people selected randomly from who retweeted their announcement tweet. I told him tweeting 'these three people are selected randomly and they won' is boring and I have an idea.

The idea was, showing a table of candidates sorted by their points. everybody starts with zero and every second systems select a random candidate and gives them 1 point, the candidate gets highlighted and and table re-sorts itself.

I was curious about Dropwizard and wanted to give it a shot. Adem was very excited about the idea and we coded it that night and released it the day after. People loved it. I currently can not find the front-end code, so it's not very useful now.

Please note that this piece of code doesn't follow any best practices whatsoever, because it's not meant to be maintained :) If you want to see a project where I tried to follow best practices, please see the hiring-challenge-scala-contacts repo.