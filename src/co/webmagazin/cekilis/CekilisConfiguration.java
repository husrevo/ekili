package co.webmagazin.cekilis;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yammer.dropwizard.config.Configuration;

public class CekilisConfiguration extends Configuration {
	@NotEmpty
    @JsonProperty
    private String inputPath;

    @NotEmpty
    @JsonProperty
    private String backupPath = "backup/";
    
    @JsonProperty
    private int maxWinners;
    
    @JsonProperty
    private int maxScore;
    
    @JsonProperty
    private int frequencyInMS;
    
    @JsonProperty
    private boolean createBackups;

	public int getMaxWinners() {
		return maxWinners;
	}

	public void setMaxWinners(int maxWinners) {
		this.maxWinners = maxWinners;
	}

	public int getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(int maxScore) {
		this.maxScore = maxScore;
	}

	public int getFrequencyInMS() {
		return frequencyInMS;
	}

	public void setFrequencyInMS(int frequencyInMS) {
		this.frequencyInMS = frequencyInMS;
	}

	public boolean isCreateBackups() {
		return createBackups;
	}

	public void setCreateBackups(boolean createBackups) {
		this.createBackups = createBackups;
	}

	public String getInputPath() {
		return inputPath;
	}

	public void setInputPath(String inputPath) {
		this.inputPath = inputPath;
	}

	public String getBackupPath() {
		return backupPath;
	}

	public void setBackupPath(String backupPath) {
		this.backupPath = backupPath;
	}
	
}
