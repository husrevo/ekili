package co.webmagazin.cekilis.core;

import java.util.concurrent.atomic.AtomicInteger;

public class Katilimci {
	private final long id;
	private final String screenName;
	private final String name;
	private final String imgUrl;
	private AtomicInteger points;
	
	public Katilimci(long id, String screen_name, String name, String img_url) {
		this.id = id;
		this.screenName = screen_name;
		this.name = name;
		this.imgUrl = img_url;
		points = new AtomicInteger(0);
	}

	public long getId() {
		return id;
	}

	public String getScreenName() {
		return screenName;
	}

	public String getName() {
		return name;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public AtomicInteger getPoints() {
		return points;
	}

	
	
	
	
}
