package co.webmagazin.cekilis.core;

public class SonOy {
	public SonOy(int sira, Katilimci katilimci) {
		this.sira = sira;
		this.katilimci = katilimci;
	}
	
	private int sira;
	private Katilimci katilimci;
	public int getSira() {
		return sira;
	}
	public Katilimci getKatilimci() {
		return katilimci;
	}
	
}
