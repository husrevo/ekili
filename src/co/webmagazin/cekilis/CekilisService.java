package co.webmagazin.cekilis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.log.Log;

import co.webmagazin.cekilis.core.Katilimci;
import co.webmagazin.cekilis.core.SonOy;
import co.webmagazin.cekilis.resources.ClearTask;
import co.webmagazin.cekilis.resources.CountTask;
import co.webmagazin.cekilis.resources.DurumResource;
import co.webmagazin.cekilis.resources.SonOyResource;
import co.webmagazin.cekilis.resources.StartTask;
import co.webmagazin.cekilis.resources.StopTask;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.Environment;
import com.yammer.dropwizard.config.FilterBuilder;

public class CekilisService extends Service<CekilisConfiguration> {

	private ArrayList<Katilimci> katilimcilar;
	private SonOy sonOy;
	private String outputPath;
	private String folderPath;
	private CountTask countTask;

	private int maxWinners;
	private int maxScore;
	private int frequency;
	private boolean createBackups;

	private long last_backup = 0;

	private int kazananlar = 0;

	Timer timer;
	Random random = new Random(System.currentTimeMillis());

	public void start() {
		sonOy = new SonOy(0, katilimcilar.get(0));
		timer = new Timer();
		timer.schedule(new CountTimerTask(), frequency, frequency);
	}

	public void stop() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}

	}

	public void clear() {
		stop();
		for (Katilimci katilimci : katilimcilar)
			katilimci.getPoints().set(0);
		sonOy = new SonOy(0, katilimcilar.get(0));
	}

	public static void main(String[] args) throws Exception {
		new CekilisService().run(args);
	}

	@Override
	public void initialize(Bootstrap<CekilisConfiguration> bootstrap) {
		bootstrap.setName("webmagazin-cekilis");
	}

	@Override
	public void run(CekilisConfiguration configuration, Environment environment)
			throws Exception {
		folderPath = configuration.getInputPath();
		outputPath = configuration.getBackupPath();
		
		maxScore = configuration.getMaxScore();
		maxWinners = configuration.getMaxWinners();
		frequency = configuration.getFrequencyInMS();
		createBackups = configuration.isCreateBackups();
		
		new File(outputPath).mkdirs();

		File yedek = new File(outputPath + "save.json");
		if (yedek.exists()) {
			Gson gson = new Gson();
			Log.info("Yedekten aliniyor.");
			Type listOfKatilimci = new TypeToken<ArrayList<Katilimci>>() {
			}.getType();
			katilimcilar = gson
					.fromJson(new FileReader(yedek), listOfKatilimci);

		}

		if (katilimcilar == null)
			katilimcilar = initialState(folderPath);

		environment.addResource(new DurumResource(this));
		environment.addResource(new SonOyResource(this));
		countTask = new CountTask(this);
		environment.addTask(countTask);
		environment.addTask(new StartTask(this));
		environment.addTask(new StopTask(this));
		environment.addTask(new ClearTask(this));

		FilterBuilder filterConfig = environment.addFilter(
				CrossOriginFilter.class, "/*");
		filterConfig.setInitParam(CrossOriginFilter.PREFLIGHT_MAX_AGE_PARAM,
				Integer.valueOf(60 * 60 * 24).toString()); // 1 day
	}

	public ArrayList<Katilimci> getKatilimcilar() {
		return katilimcilar;
	}

	private static ArrayList<Katilimci> initialState(String folderPath)
			throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		Log.info("Bastan cekiliyor.");

		HashMap<String, Katilimci> map = new HashMap<String, Katilimci>();
		JsonParser parser = new JsonParser();

		File folder = new File(folderPath);
		File[] jsonFiles = folder.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File arg0, String arg1) {

				return arg1.endsWith(".json");
			}
		});

		for (File file : jsonFiles) {
			JsonElement icerik = parser.parse(new FileReader(file));
			if (icerik.isJsonArray()) {
				JsonArray jarray = (JsonArray) icerik;
				for (JsonElement je : jarray) {
					JsonObject jo = (JsonObject) je;
					long id = jo.get("id").getAsLong();
					String name = jo.get("name").getAsString();

					String screenName = null;
					String url = null;
					try {
						screenName = jo.get("screen_name").getAsString();
					} catch (Exception e) {
					}
					try {
						url = jo.get("profile_image_url").getAsString();
					} catch (Exception e) {
					}

					Katilimci katilimci = new Katilimci(id, screenName, name,
							url);
					map.put(name, katilimci);
				}
			}
		}

		ArrayList<Katilimci> katilimcilar = new ArrayList<Katilimci>();
		for (String key : map.keySet()) {
			katilimcilar.add(map.get(key));
		}
		return katilimcilar;

	}

	public SonOy getSonOy() {
		return sonOy;
	}

	public void resort() {
		Collections.sort(katilimcilar, new Comparator<Katilimci>() {

			@Override
			public int compare(Katilimci o1, Katilimci o2) {
				return o2.getPoints().intValue() - o1.getPoints().intValue();

			}
		});

		if(createBackups && last_backup < System.currentTimeMillis() - 2000) {
				createBackup();
		}

	}

	private void createBackup() {
		last_backup = System.currentTimeMillis();
		File output = new File(outputPath + "save.json");
		if (output.exists()) {
			output.renameTo(new File(outputPath + "old_save.json"));
			output = new File(outputPath + "save.json");
		}

		try {
			Gson gson = new Gson();
			String save = gson.toJson(katilimcilar);

			FileWriter fw = new FileWriter(output);
			fw.write(save);
			fw.close();
		} catch (IOException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void processCount(Katilimci secilenKatilimci) {
		int deger = secilenKatilimci.getPoints().incrementAndGet();

		resort();
		sonOy = new SonOy(katilimcilar.indexOf(secilenKatilimci),
				secilenKatilimci);
		if (deger == maxScore)
			kazananlar++;

		if (kazananlar >= maxWinners)
			timer.cancel();

	}

	private class CountTimerTask extends TimerTask {

		@Override
		public void run() {
			int max = getKatilimcilar().size();
			Katilimci secilenKatilimci = null;
			do {
				int secilen = random.nextInt(max);
				secilenKatilimci = getKatilimcilar().get(secilen);
			} while (secilenKatilimci.getPoints().intValue() >= 100);

			Log.info("secilen kullanici: " + secilenKatilimci.getScreenName()
					+ " (" + secilenKatilimci.getPoints().get() + ")\n");
			processCount(secilenKatilimci);

		}

	}
}
