package co.webmagazin.cekilis.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import co.webmagazin.cekilis.CekilisService;
import co.webmagazin.cekilis.core.SonOy;

import com.yammer.metrics.annotation.Timed;

@Path("/sonOy")
@Produces(MediaType.APPLICATION_JSON)
public class SonOyResource {
	private final CekilisService service;
	public SonOyResource(CekilisService cekilisService) {
		service = cekilisService;
	}
	
	@GET
	@Timed
	public SonOy getSonOy(){
		return service.getSonOy();
	}
}
