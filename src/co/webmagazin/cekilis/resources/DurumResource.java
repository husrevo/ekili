package co.webmagazin.cekilis.resources;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import co.webmagazin.cekilis.CekilisService;
import co.webmagazin.cekilis.core.Katilimci;

import com.yammer.metrics.annotation.Timed;


@Path("/durum")
@Produces(MediaType.APPLICATION_JSON)
public class DurumResource {
	private final CekilisService service;
	public DurumResource(CekilisService cekilisService) {
		service = cekilisService;
	}
	
	@GET
	@Timed
	public ArrayList<Katilimci> getDurum(){
		return service.getKatilimcilar();
	}
	
	
}
