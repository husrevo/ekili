package co.webmagazin.cekilis.resources;

import java.io.PrintWriter;
import java.util.Random;

import co.webmagazin.cekilis.CekilisService;
import co.webmagazin.cekilis.core.Katilimci;

import com.google.common.collect.ImmutableMultimap;
import com.yammer.dropwizard.tasks.Task;

public class CountTask extends Task {
	private final CekilisService service;
	Random random = new Random(System.currentTimeMillis());
	
	public CountTask(CekilisService service) {
		super("count-task");
		this.service = service;
		
	}
	
	@Override
	public void execute(ImmutableMultimap<String, String> arg0, PrintWriter writer) throws Exception {
		int max = service.getKatilimcilar().size();
		int secilen = random.nextInt(max);
		Katilimci secilenKatilimci = service.getKatilimcilar().get(secilen);
		writer.append("secilen sayi: "+secilen+"\n");
		writer.append("secilen kullanici: " + secilenKatilimci.getScreenName()+"\n");
		service.processCount(secilenKatilimci);
		
	}
	
}
