package co.webmagazin.cekilis.resources;

import java.io.PrintWriter;
import java.util.Random;

import co.webmagazin.cekilis.CekilisService;

import com.google.common.collect.ImmutableMultimap;
import com.yammer.dropwizard.tasks.Task;

public class StopTask extends Task {

	private final CekilisService service;
	Random random = new Random(System.currentTimeMillis());
	
	public StopTask(CekilisService service) {
		super("stop");
		this.service = service;
		
	}
	
	@Override
	public void execute(ImmutableMultimap<String, String> parameters,
			PrintWriter output) throws Exception {
		service.stop();
	}

}
