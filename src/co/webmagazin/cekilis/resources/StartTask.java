package co.webmagazin.cekilis.resources;

import java.io.PrintWriter;
import java.util.Random;

import co.webmagazin.cekilis.CekilisService;

import com.google.common.collect.ImmutableMultimap;
import com.yammer.dropwizard.tasks.Task;

public class StartTask extends Task {

	private final CekilisService service;
	Random random = new Random(System.currentTimeMillis());
	
	public StartTask(CekilisService service) {
		super("start");
		this.service = service;
		
	}
	
	@Override
	public void execute(ImmutableMultimap<String, String> parameters,
			PrintWriter output) throws Exception {
		service.start();
	}

}
